package com.app.unitconverter.main.utils;

import com.app.unitconverter.abstraction.exceptions.CustomException;

import java.util.ArrayList;
import java.util.List;

public class Validate {

    private static final List<String> LETTERS = List.of("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O");

    public static void validateLengthDecimal(String decimalNumber){
        if (decimalNumber.length() > 19) {
            throw new CustomException("El valor decimal es demasiado grande");
        }
    }

    public static void validateBasePower(int basePower){
        if (basePower != 2 && basePower != 8 && basePower != 10 && basePower != 16) {
            throw new CustomException("La base numérica no es válida");
        }
    }

    public static void validateNumericSystem(int basePower, String numericValue) {
        switch (basePower) {
            case 2 -> validateBinaryNumber(numericValue);
            case 8 -> validateOctalNumber(numericValue);
            case 10 -> validateDecimalNumber(numericValue);
            case 16 -> validateHexadecimalNumber(numericValue);
        }
    }

    public static void validateGenNumericSystem(int basePower, String numericValue){
        List<String> allowedDigits = getAllowedDigits(basePower);
        for (int i = 0; i < numericValue.length(); i++) {
            if (!allowedDigits.contains(String.valueOf(numericValue.charAt(i)))) {
                throw new CustomException(String.format("El valor ingresado no pertenece al sistema de base numérica %d", basePower));
            }
        }
    }

    private static List<String> getAllowedDigits(Integer basePower) {
        List<String> digits = new ArrayList<>();
        for (int i = 0; i < basePower; i++) {
            if (i >= 10) {
                digits.add(LETTERS.get(i - 10));
            } else {
                digits.add(String.valueOf(i));
            }
        }
        return digits;
    }

    private static void validateDecimalNumber(String numericValue) {
        if (!numericValue.matches("-?[0-9]+")) {
            throw new CustomException("El valor ingresado no es un número decimal");
        }
    }

    private static void validateOctalNumber(String numericValue) {
        if (!numericValue.matches("-?[0-7]+")){
            throw new CustomException("El valor ingresado no es un número octal");
        }
    }

    private static void validateHexadecimalNumber(String numericValue) {
        if (!numericValue.matches("-?[0-9a-fA-F]+")){
            throw new CustomException("El valor ingresado no es un número hexadecimal");
        }
    }

    private static void validateBinaryNumber(String numericValue) {
        if (!numericValue.matches("-?[0-1]+")) {
            throw new CustomException("El valor ingresado no es un número binario");
        }
    }
}
