package com.app.unitconverter.main.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class MultiplyBinary {

    public static List<Integer> listFactor1;
    public static List<Integer> listFactor2;
    public static List<List<Integer>> results;
    public static List<Integer> sumResult;
    public static List<Integer> accumulatedNumbers;

    public static String convert(String number1, String number2) {
        init();
        listFactor1 = getListNumber(number1);
        listFactor2 = getListNumber(number2);
        getResults();
        getSumResult();
        return getResult(sumResult);
    }

    private static void init() {
        listFactor1 = new ArrayList<>();
        listFactor2 = new ArrayList<>();
        results = new ArrayList<>();
        sumResult = new ArrayList<>();
        accumulatedNumbers = new ArrayList<>();
    }

    private static Stack<Integer> getListNumber(String number) {
        Stack<Integer> list = new Stack<>();
        for (int i = 0; i < number.length(); i++) {
            list.add(Integer.parseInt(String.valueOf(number.charAt(i))));
        }
        return list;
    }

    private static void getResults() {
        Collections.reverse(listFactor2);
        for (int i = 0; i < listFactor2.size(); i++) {
            List<Integer> partialResult = new ArrayList<>();
            for (Integer integer : listFactor1) {
                partialResult.add(listFactor2.get(i) * integer);
            }
            addZerosRight(partialResult, i);
            results.add(partialResult);
        }
        Collections.reverse(listFactor2);
        addZerosLeft();
    }

    private static void addZerosRight(List<Integer> partialResult, int index) {
        for (int i = 0; i < index; i++) {
            partialResult.add(0);
        }
    }

    private static void addZerosLeft() {
        for (List<Integer> list : results) {
            Collections.reverse(list);
            int zerosToAdd = results.get(results.size() - 1).size() - list.size();
            if (zerosToAdd != 0) {
                for (int i = 0; i < zerosToAdd; i++) {
                    list.add(0);
                }
            }
            Collections.reverse(list);
        }
    }

    private static void getSumResult() {
        accumulatedNumbers.add(0);
        for (int i = results.get(0).size() - 1; i >= 0; i--) {
            int partialSum = 0;
            for (List<Integer> list : results) {
                partialSum += list.get(i);
            }
            partialSum += +accumulatedNumbers.get(accumulatedNumbers.size() - 1);
            getSumBinary(partialSum);
            if (i == 0 && accumulatedNumbers.get(accumulatedNumbers.size() - 1) == 1) {
                sumResult.add(1);
            }
        }
        Collections.reverse(sumResult);
    }

    private static void getSumBinary(int result) {
        switch (result) {
            case 0 -> {
                sumResult.add(0);
                accumulatedNumbers.add(0);
            }
            case 1 -> {
                sumResult.add(1);
                accumulatedNumbers.add(0);
            }
            case 2 -> {
                sumResult.add(0);
                accumulatedNumbers.add(1);
            }
            case 3 -> {
                sumResult.add(1);
                accumulatedNumbers.add(1);
            }
            default -> {
                if (result % 2 != 0) {
                    sumResult.add(1);
                } else {
                    sumResult.add(0);
                }
                accumulatedNumbers.add(result / 2);
            }
        }
    }

    private static String getNumberString(List<Integer> listFactor) {
        StringBuilder resultSum = new StringBuilder();
        for (Integer integer : listFactor) {
            resultSum.append(integer.toString()).append(" ");
        }
        return String.valueOf(resultSum);
    }

    private static String getResult(List<Integer> listFactor) {
        StringBuilder result = new StringBuilder();
        for (Integer integer : listFactor) {
            result.append(integer.toString());
        }
        return String.valueOf(result);
    }

    public static List<String> getProcess() {
        List<String> process = new ArrayList<>();
        process.add("Multiplicación de $" + getNumberString(listFactor1) + " * " + getNumberString(listFactor2) + "$:");
        Collections.reverse(listFactor2);
        for (int i = 0; i < listFactor2.size(); i++) {
            process.add("$" + getNumberString(listFactor1) + " \\hspace{0.1cm} * \\hspace{0.1cm} \\textbf{" + listFactor2.get(i) + "} \\hspace{0.1cm} = \\hspace{0.1cm} \\textit{ " + getNumberString(results.get(i)) + "}$");
        }
        process.add("$Resultado \\hspace{0.1cm}final: \\hspace{0.1cm} \\textbf{" + getNumberString(sumResult) + "}$");
        Collections.reverse(listFactor2);
        return process;
    }

}
