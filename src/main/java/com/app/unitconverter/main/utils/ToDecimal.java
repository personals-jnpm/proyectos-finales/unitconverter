package com.app.unitconverter.main.utils;

import java.util.*;

public class ToDecimal {

    private static List<Integer> listNumber;
    private static Queue<Long> listPowers;

    public static String convert(String number, int basePower) {
        init();
        getListNumber(number);
        getListPowers(number.length() + 1, basePower);
        return getDecimalNumber() + "";
    }

    private static void init() {
        listNumber = new Stack<>();
        listPowers = new LinkedList<>();
    }

    private static void getListNumber(String number) {
        for (int i = 0; i < number.length(); i++) {
            char character = number.charAt(i);
            switch (character) {
                case 'A', 'a' -> listNumber.add(10);
                case 'B', 'b' -> listNumber.add(11);
                case 'C', 'c' -> listNumber.add(12);
                case 'D', 'd' -> listNumber.add(13);
                case 'E', 'e' -> listNumber.add(14);
                case 'F', 'f' -> listNumber.add(15);
                case 'G', 'g' -> listNumber.add(16);
                case 'H', 'h' -> listNumber.add(17);
                case 'I', 'i' -> listNumber.add(18);
                case 'J', 'j' -> listNumber.add(19);
                case 'K', 'k' -> listNumber.add(20);
                case 'L', 'l' -> listNumber.add(21);
                case 'M', 'm' -> listNumber.add(22);
                case 'N', 'n' -> listNumber.add(23);
                case 'O', 'o' -> listNumber.add(24);
                default -> listNumber.add(Integer.parseInt(String.valueOf(number.charAt(i))));
            }
        }
    }

    private static void getListPowers(int maxPower, int basePower) {
        for (int i = 0; i < maxPower; i++) {
            listPowers.add((long) Math.pow(basePower, i));
        }
    }

    private static long getDecimalNumber() {
        long resultDecimal = 0;
        List<Integer> newListNumber = new ArrayList<>(listNumber);
        Queue<Long> newListPowers = new LinkedList<>(listPowers);

        Collections.reverse(newListNumber);
        for (Integer integer : newListNumber) {
            try {
                resultDecimal += integer * newListPowers.poll();
            } catch (NullPointerException ignored) {
            }
        }
        return resultDecimal;
    }

    public static List<String> getProcess(String result) {
        String process = "$";
        List<Integer> newListNumber = new ArrayList<>(listNumber);
        Queue<Long> newListPowers = new LinkedList<>(listPowers);

        Collections.reverse(newListNumber);
        for (int i = 0; i < newListNumber.size(); i++) {
            try {
                process = process.concat(((i == 0) ? "" : " + ") + "(" + newListNumber.get(i) + " * " +
                        newListPowers.poll() + ")");
            } catch (NullPointerException ignored) {
            }
        }
        return List.of(process.concat(" = " + result + "$"));
    }

}
