package com.app.unitconverter.main.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class NumericSystem {

    private static Stack<Long> quotients;
    private static List<Long> residues;

    public static String convert(String numericValue, int baseNumber) {
        init();
        long number = Long.parseLong(numericValue);

        do {
            residues.add(number % baseNumber);
            quotients.push(number / baseNumber);
            number = number / baseNumber;
        } while (quotients.lastElement() >= 1);

        Collections.reverse(residues);
        quotients.pop();
        return getNumber(residues);
    }

    private static void init() {
        quotients = new Stack<>();
        residues = new ArrayList<>();
    }

    public static List<String> getProcess(String originalNumber, int baseNumber) {
        List<String> process = new ArrayList<>();
        Collections.reverse(residues);
        try {
            for (int i = 0; i < quotients.size(); i++) {
                if (i == 0) {
                    process.add("$\\frac{" + originalNumber + "}{" + baseNumber + "} = " + quotients.get(i) +
                            ", \\hspace{0.3cm} Residuo: \\textbf{" + residues.get(i) + "}$");
                }
                if (quotients.get(i) > 1) {
                    process.add("$\\frac{" + quotients.get(i) + "}{" + baseNumber + "} = " + quotients.get(i + 1) +
                            ", \\hspace{0.3cm} Residuo: \\textbf{" + residues.get(i + 1) + "}$");
                }
            }
            Collections.reverse(residues);
            process.add("El número " + originalNumber + " en sistema " + getSystemName(baseNumber) + " es " +
                    getNumber(residues));
        } catch (Exception ignored) {
        }
        return process;
    }

    private static String getNumber(List<Long> residues) {
        String number = "";
        for (Long value : residues) {
            if (value == 10) {
                number = number.concat("A");
            } else if (value == 11) {
                number = number.concat("B");
            } else if (value == 12) {
                number = number.concat("C");
            } else if (value == 13) {
                number = number.concat("D");
            } else if (value == 14) {
                number = number.concat("E");
            } else if (value == 15) {
                number = number.concat("F");
            } else if (value == 16) {
                number = number.concat("G");
            } else if (value == 17) {
                number = number.concat("H");
            } else if (value == 18) {
                number = number.concat("I");
            } else if (value == 19) {
                number = number.concat("J");
            } else if (value == 20) {
                number = number.concat("K");
            } else if (value == 21) {
                number = number.concat("L");
            } else if (value == 22) {
                number = number.concat("M");
            } else if (value == 23) {
                number = number.concat("N");
            } else if (value == 24) {
                number = number.concat("O");
            } else {
                number = number.concat(value.toString());
            }
        }
        return number;
    }

    private static String getSystemName(int baseNumber) {
        String nameSystem;
        switch (baseNumber) {
            case 2 -> nameSystem = "binario";
            case 8 -> nameSystem = "octal";
            case 16 -> nameSystem = "hexadecimal";
            default -> nameSystem = "no definido";
        }
        return nameSystem;
    }

}
