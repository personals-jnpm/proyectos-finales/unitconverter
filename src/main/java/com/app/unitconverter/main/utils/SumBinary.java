package com.app.unitconverter.main.utils;

import com.app.unitconverter.abstraction.exceptions.CustomException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class SumBinary {

    public static List<Integer> result;
    public static List<Integer> accumulatedNumbers;
    public static Stack<Integer> numberList1;
    public static Stack<Integer> numberList2;

    public static String convert(String number1, String number2, String typeOperation) {
        init();
        numberList1 = getListNumber(number1);
        numberList2 = getListNumber(number2);
        fillNoneValues();

        if (typeOperation.equals("suma")) {
            getSumBinaryNumbers(numberList1.size());
        } else if (typeOperation.equals("resta")) {
            getSubtractionBinaryNumbers(numberList1.size());
        } else {
            throw new CustomException("El tipo de operación no es válido");
        }

        return getResult();
    }

    private static void init() {
        result = new ArrayList<>();
        accumulatedNumbers = new ArrayList<>();
        numberList1 = new Stack<>();
        numberList2 = new Stack<>();
    }

    public static Stack<Integer> getListNumber(String number) {
        Stack<Integer> list = new Stack<>();
        for (int i = 0; i < number.length(); i++) {
            list.add(Integer.parseInt(String.valueOf(number.charAt(i))));
        }
        return list;
    }

    private static void fillNoneValues() {
        Collections.reverse(numberList1);
        Collections.reverse(numberList2);
        Stack<Integer> minorList = (numberList1.size() <= numberList2.size()) ? numberList1 : numberList2;
        for (int i = 0; i < (Math.max(numberList1.size(), numberList2.size())); i++) {
            try {
                int number = minorList.get(i);
            } catch (Exception e) {
                minorList.add(0);
            }
        }
        Collections.reverse(numberList1);
        Collections.reverse(numberList2);
    }

    private static void getSumBinaryNumbers(int size) {
        Stack<Integer> list1 = new Stack<>();
        Stack<Integer> list2 = new Stack<>();

        list1.addAll(numberList1);
        list2.addAll(numberList2);

        accumulatedNumbers.add(0);
        for (int i = 0; i < size; i++) {
            int partialResult = list1.get(list1.size() - 1) + list2.get(list2.size() - 1)
                    + accumulatedNumbers.get(accumulatedNumbers.size() - 1);
            switch (partialResult) {
                case 0 -> {
                    result.add(0);
                    accumulatedNumbers.add(0);
                }
                case 1 -> {
                    result.add(1);
                    accumulatedNumbers.add(0);
                }
                case 2 -> {
                    result.add(0);
                    accumulatedNumbers.add(1);
                }
                case 3 -> {
                    result.add(1);
                    accumulatedNumbers.add(1);
                }
            }
            if (i == size - 1 && accumulatedNumbers.get(accumulatedNumbers.size() - 1) == 1) {
                result.add(1);
            }
            list1.pop();
            list2.pop();
        }
        Collections.reverse(result);
    }

    public static void getSubtractionBinaryNumbers(int size) {
        Stack<Integer> list1 = new Stack<>();
        Stack<Integer> list2 = new Stack<>();

        list1.addAll(numberList1);
        list2.addAll(numberList2);

        accumulatedNumbers.add(0);
        for (int i = 0; i < size; i++) {
            int partialResult = list1.get(list1.size() - 1) - list2.get(list2.size() - 1)
                    - accumulatedNumbers.get(accumulatedNumbers.size() - 1);
            switch (partialResult) {
                case -1 -> {
                    result.add(1);
                    accumulatedNumbers.add(1);
                }
                case 0 -> {
                    result.add(0);
                    accumulatedNumbers.add(0);
                }
                case 1 -> {
                    result.add(1);
                    accumulatedNumbers.add(0);
                }
            }
            if (i == size - 1 && accumulatedNumbers.get(accumulatedNumbers.size() - 1) == 1) {
                result.add(1);
            }
            list1.pop();
            list2.pop();
        }
        Collections.reverse(result);
    }

    private static String getResult() {
        StringBuilder resultSum = new StringBuilder();
        for (Integer integer : result) {
            resultSum.append(integer.toString());
        }
        return String.valueOf(resultSum);
    }

    public static List<String> getProcess(String number1, String number2, String typeOperation) {
        List<String> process = new ArrayList<>();

        Stack<Integer> list1 = new Stack<>();
        Stack<Integer> list2 = new Stack<>();

        list1.addAll(numberList1);
        list2.addAll(numberList2);

        Collections.reverse(list1);
        Collections.reverse(list2);

        String sign;

        if (typeOperation.equals("suma")) {
            process.add("Suma de $\\textbf{" + number1 + " + " + number2 + "}$:");
            sign = "+";
        } else {
            process.add("Resta de $\\textbf{" + number1 + " - " + number2 + "}$:");
            sign = "-";
        }

        for (int i = 0; i < result.size(); i++) {
            try {
                process.add("$[" + accumulatedNumbers.get(i) + "] \\hspace{0.3cm} " + list1.get(i) + " " + sign +
                        " " + list2.get(i) + " = \\textbf{" + result.get(result.size() - i - 1) + "}$");
            } catch (Exception e) {
                process.add("$..................\\hspace{0.2cm} \\textbf{" +
                        result.get(result.size() - i - 1) + "}$");
            }
        }
        process.add("El resultado de la " + typeOperation + " es: $\\textbf{" + getResult() + "}$");
        return process;
    }

}
