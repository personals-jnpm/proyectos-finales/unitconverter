package com.app.unitconverter.main.api;

import com.app.unitconverter.abstraction.dto.request.ConverterReqDto;
import com.app.unitconverter.abstraction.dto.request.ConverterReqGen;
import com.app.unitconverter.abstraction.dto.request.ConverterReqOpDto;
import com.app.unitconverter.abstraction.dto.response.ConverterResDto;
import com.app.unitconverter.abstraction.dto.response.ConverterResOpDto;
import com.app.unitconverter.abstraction.services.IServiceConverter;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin(maxAge = 3600)
@RequestMapping("/api")
public class ConverterController {

    private final IServiceConverter service;

    public ConverterController(IServiceConverter service) {
        this.service = service;
    }

    @PostMapping("/to-decimal")
    public ConverterResDto convertToDecimal(@Valid @RequestBody ConverterReqDto converter){
        return service.toDecimal(converter);
    }

    @PostMapping("/decimal-to")
    public ConverterResDto convertDecimalTo(@Valid @RequestBody ConverterReqDto converter){
        return service.decimalTo(converter);
    }

    @PostMapping("/sum-binary")
    public ConverterResOpDto sumBinaries(@Valid @RequestBody ConverterReqOpDto converter){
        return service.sumBinary(converter);
    }

    @PostMapping("/multiply-binary")
    public ConverterResOpDto multiplyBinaries(@Valid @RequestBody ConverterReqOpDto converter){
        return service.multiplyBinary(converter);
    }

    @PostMapping("/generic-converter")
    public ConverterResDto genericConvertor(@Valid @RequestBody ConverterReqGen converter) {
        return service.genericConverter(converter);
    }
}
