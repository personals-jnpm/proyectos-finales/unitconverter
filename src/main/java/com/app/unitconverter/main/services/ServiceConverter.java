package com.app.unitconverter.main.services;

import com.app.unitconverter.abstraction.dto.request.ConverterReqDto;
import com.app.unitconverter.abstraction.dto.request.ConverterReqGen;
import com.app.unitconverter.abstraction.dto.request.ConverterReqOpDto;
import com.app.unitconverter.abstraction.dto.response.ConverterResDto;
import com.app.unitconverter.abstraction.dto.response.ConverterResOpDto;
import com.app.unitconverter.abstraction.services.IServiceConverter;
import com.app.unitconverter.main.utils.*;
import org.springframework.stereotype.Service;

@Service
public class ServiceConverter implements IServiceConverter {

    @Override
    public ConverterResDto decimalTo(ConverterReqDto requestDto) {
        Validate.validateBasePower(requestDto.getBasePower());
        Validate.validateNumericSystem(10, requestDto.getNumericValue());
        Validate.validateLengthDecimal(requestDto.getNumericValue());

        return ConverterResDto.builder()
                .numericValue(requestDto.getNumericValue())
                .resultValue(NumericSystem.convert(requestDto.getNumericValue(), requestDto.getBasePower()))
                .process(NumericSystem.getProcess(requestDto.getNumericValue(), requestDto.getBasePower()))
                .build();
    }

    @Override
    public ConverterResDto toDecimal(ConverterReqDto requestDto) {
        Validate.validateBasePower(requestDto.getBasePower());
        Validate.validateNumericSystem(requestDto.getBasePower(), requestDto.getNumericValue());

        String result = ToDecimal.convert(requestDto.getNumericValue(), requestDto.getBasePower());
        return ConverterResDto.builder()
                .numericValue(requestDto.getNumericValue())
                .resultValue(result)
                .process(ToDecimal.getProcess(result))
                .build();
    }

    @Override
    public ConverterResOpDto sumBinary(ConverterReqOpDto reqOpDto) {
        return ConverterResOpDto.builder()
                .numericValueOne(reqOpDto.getNumericValueOne())
                .numericValueTwo(reqOpDto.getNumericValueTwo())
                .resultValue(SumBinary.convert(reqOpDto.getNumericValueOne(), reqOpDto.getNumericValueTwo(),
                        reqOpDto.getTypeOperation()))
                .process(SumBinary.getProcess(reqOpDto.getNumericValueOne(), reqOpDto.getNumericValueTwo(),
                        reqOpDto.getTypeOperation()))
                .build();
    }

    @Override
    public ConverterResOpDto multiplyBinary(ConverterReqOpDto reqOpDto) {
        return ConverterResOpDto.builder()
                .numericValueOne(reqOpDto.getNumericValueOne())
                .numericValueTwo(reqOpDto.getNumericValueTwo())
                .resultValue(MultiplyBinary.convert(reqOpDto.getNumericValueOne(), reqOpDto.getNumericValueTwo()))
                .process(MultiplyBinary.getProcess())
                .build();
    }

    @Override
    public ConverterResDto genericConverter(ConverterReqGen requestDto) {
        Validate.validateGenNumericSystem(Integer.parseInt(requestDto.getOriginBasePower()), requestDto.getNumericValue());
        String decimalResult = ToDecimal.convert(requestDto.getNumericValue(), Integer.parseInt(requestDto.getOriginBasePower()));
        return ConverterResDto.builder()
                .numericValue(requestDto.getNumericValue())
                .resultValue(NumericSystem.convert(decimalResult, Integer.parseInt(requestDto.getFinalBasePower())))
                .build();
    }
}
