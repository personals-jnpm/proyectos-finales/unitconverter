package com.app.unitconverter.abstraction.dto.request;

import lombok.Data;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class ConverterReqGen {

    @Pattern(regexp = "-?[0-9a-pA-P]+", message = "no es válido")
    @Size(min = 1, max = 30)
    private String numericValue;

    @Pattern(regexp = "^([2-9]|1[0-9]|2[0-5])$", message = "no es válido")
    @Size(min = 1, max = 30)
    private String originBasePower;

    @Pattern(regexp = "^([2-9]|1[0-9]|2[0-5])$", message = "no es válido")
    @Size(min = 1, max = 30)
    private String finalBasePower;
}
