package com.app.unitconverter.abstraction.dto.request;

import lombok.Data;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class ConverterReqDto {

    @Pattern(regexp = "-?[0-9a-fA-F]+", message = "no es válido")
    @Size(min = 1, max = 30)
    private String numericValue;
    private int basePower;
}
