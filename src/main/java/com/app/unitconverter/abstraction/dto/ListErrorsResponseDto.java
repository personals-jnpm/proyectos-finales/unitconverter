package com.app.unitconverter.abstraction.dto;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class ListErrorsResponseDto {

    @Getter
    private final List<String> errors;

    public ListErrorsResponseDto() {
        this.errors = new ArrayList<>();
    }

    public void add(String dto){
        this.errors.add(dto);
    }

}
