package com.app.unitconverter.abstraction.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ConverterResOpDto {

    private String numericValueOne;
    private String numericValueTwo;
    private String resultValue;
    private List<String> process;

}
