package com.app.unitconverter.abstraction.dto.request;

import lombok.Data;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class ConverterReqOpDto {

    @Pattern(regexp = "-?[0-1]+", message = "no es un número binario")
    @Size(min = 1)
    private String numericValueOne;

    @Pattern(regexp = "-?[0-1]+", message = "no es un número binario")
    @Size(min = 1)
    private String numericValueTwo;

    private String typeOperation;
}
