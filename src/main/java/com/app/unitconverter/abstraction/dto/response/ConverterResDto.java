package com.app.unitconverter.abstraction.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ConverterResDto {

    private String numericValue;
    private String resultValue;
    private List<String> process;

}
