package com.app.unitconverter.abstraction.services;

import com.app.unitconverter.abstraction.dto.request.ConverterReqDto;
import com.app.unitconverter.abstraction.dto.request.ConverterReqGen;
import com.app.unitconverter.abstraction.dto.request.ConverterReqOpDto;
import com.app.unitconverter.abstraction.dto.response.ConverterResDto;
import com.app.unitconverter.abstraction.dto.response.ConverterResOpDto;

public interface IServiceConverter {

    ConverterResDto decimalTo(ConverterReqDto requestDto);
    ConverterResDto toDecimal(ConverterReqDto requestDto);
    ConverterResOpDto sumBinary(ConverterReqOpDto reqOpDto);
    ConverterResOpDto multiplyBinary(ConverterReqOpDto reqOpDto);
    ConverterResDto genericConverter(ConverterReqGen requestDto);
}
